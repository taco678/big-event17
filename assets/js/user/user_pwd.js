$(function () {
  //1.注册 自定义 校验规则 ------------------------------
  layui.form.verify({
    pwd: [/^[\S]{6,12}$/, '亲，密码必须在6-12位哦~~ : )'],
    // 新旧密码必须不一样 规则
    samepwd(newPwd) {
      if (newPwd === $('input[name=oldPwd]').val().trim()) {
        return '对不起，新旧密码不能一样 哦~~！'
      }
    },
    confirmpwd(confirmPwd) {
      if (confirmPwd !== $('input[name=newPwd]').val().trim()) {
        return '对不起，两次密码输入不一致哦~~'
      }
    }
  })
  //2.为表单注册提交事件 ------------------------------
  $('.layui-form').on('submit',function(e){
    e.preventDefault()
    const data = layui.form.val('userForm')
    $.ajax({
      method:'post',
      url:'/my/updatepwd',
      data,
      success(res){
        if(res.status===0){
          layui.layer.msg(res.message,{icon:-1,time:666,function(){
            sessionStorage.removeItem('toeken')
            window.top.location.replace('/login.html')
          }})
        }else{
          layui.layer.msg(res.message)
        }
      }
    })
  })
})