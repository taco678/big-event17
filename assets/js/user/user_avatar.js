$(function () {
  // 1.1 获取裁剪区域的 DOM 元素
  const $image = $('#image')
  // 1.2 配置选项
  const options = {
    // 纵横比
    aspectRatio: 1,
    // 指定预览区域
    preview: '.img-preview'
  }
  // 1.3 创建裁剪区域
  $image.cropper(options)


  // 2.上传按钮 --------------------------------------------
  $('#btnCho').on('click', function () {
    $('#fileCho').trigger('click')
  })


  // 3.文件选择框 change事件----------------------------------
  // 触发时机：当 选中的文件 发生改变时 触发 
  // 为文件选择框绑定 change 事件
  $('#fileCho').on('change', function (e) {
    // 获取用户选择的文件
    // console.log(e.target.files.length);
    if (e.target.files.length === 0) {
      return layui.layer.msg('请选择照片！')
    }
    // 1. 拿到用户选择的文件
    const file = e.target.files[0]
    // 2. 将文件，转化为路径
    const imgURL = URL.createObjectURL(file)
    // 3. 重新初始化裁剪区域
    $image
      .cropper('destroy') // 销毁旧的裁剪区域
      .attr('src', imgURL) // 重新设置图片路径
      .cropper(options) // 重新初始化裁剪区域
  })


  // 4.确定上传按钮 --------------------------------------------
  $('#btnYes').on('click', function () {
    let dataURL = $image
      .cropper('getCroppedCanvas', {
        // 创建一个 Canvas 画布
        width: 100,
        height: 100
      })
      .toDataURL('image/png')
    //发请求
    $.ajax({
      method: 'post',
      url: '/my/update/avatar',
      data: {
        avatar: dataURL
      },
      success(res) {
        if (res.status != 0) {
          return layui.layer.msg('更新头像失败！')
        }
        layer.msg('更换头像成功！')
        window.parent.getUserInfo()
      }
    })
  })


})