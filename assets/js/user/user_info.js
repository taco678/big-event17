
$(function () {
  // 1.添加 自定义 表单校验规则 --------------------
  layui.form.verify({
    nickname: [/^[\S]{1,8}$/, '密码必须在1~6位之间~~'],
  })

  // 2.定义获取用户信息的方法 ----------------------
  getUserInfo()
  function getUserInfo() {
    $.ajax({
      method: 'get',
      url: '/my/userinfo',
      // headers: {
      //     Authorization: sessionStorage.getItem('token') || ''
      // },
      success: function (res) {
        console.log(res);
        if (res.status === 0) {
          layui.form.val('userForm', res.data)
        }
      }
    })
  }


  // 3.重置按钮 -----------------------------------
  $('#btnReset').on('click', function(){
    console.log($('.layui-form')[0]);
    $('.layui-form')[0].reset()
  })

  // 4.为表单 注册 提交事件 -------------------------
  $('.layui-form').on('submit', function (e) {
    e.preventDefault()
    const data = layui.form.val('userForm')
    $.ajax({
      method: 'post',
      url: '/my/userinfo',
      data,
      success(res) {
        if (res.status === 0) {
          layui.layer.msg(res.message)
        }
      }
    })
  })
})