function getUserInfo() {
    $.ajax({
        method: 'get',
        url: '/my/userinfo',
        // headers: {
        //     Authorization: sessionStorage.getItem('token') || ''
        // },
        error() { },
        success: function (res) {
            // console.log(res);
            renderAvatar(res.data)
        }
    })
}


// 入口函数
$(function () {
    getUserInfo()
})

//渲染头像
function renderAvatar(uinfo) {
    //获取用户昵称或者名字
    const uName = uinfo.nickname || uinfo.username
    $('#welcome').text('欢迎：' + uName)
    //如果有图片头像就显示图片头像
    if (uinfo.user_pic) {
        $('.layui-nav-img').attr('src', uinfo.user_pic).show()
        $('.text-avatar').hide()
    } else {
        //没有图片头像就显示名字大写首字母
        const first = uName[0].toUpperCase()
        $('.layui-nav-img').hide()
        $('.text-avatar').text(first).show()
    }
}

//退出按钮
$('#btnLogout').on('click', function () {
    //询问用户是否退出
    layui.layer.confirm('确定退出吗？', function () {
        //退出后清除token
        sessionStorage.removeItem('token')
        //然后跳转到登录页面
        location.replace('/login.html')
    })
})