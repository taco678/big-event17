// ajaxPrefilter 方法 注册了一个回调函数
//     此回调函数 会在 使用 jq的 异步请求方法时，执行


//作用：在发送异步请求之前，修改 ajax配置对象
$.ajaxPrefilter((opt) => {
  opt.url = 'http://api-breakingnews-web.itheima.net' + opt.url
  if (opt.url.indexOf('/my/') > -1) {
    if (!sessionStorage.getItem('token')) {
      alert('登录失败！')
      location.replace('/login.html')
    } else {
      opt.headers = {
        Authorization: sessionStorage.getItem('token')
      }
    }
  }

  opt.complete = function (res) {
    if (res.responseJSON.status === 1 && res.responseJSON.message === '身份认证失败！') {
      layui.layer.msg(res.message, {
        icon: 2,
        time: 1000
      }, function () {
        //如果认证失败
        // 跳转到登录页面
        location.replace('login.html')
        //移除本地token值
        sessionStorage.removeItem('token')
      })
    }
  }
})
