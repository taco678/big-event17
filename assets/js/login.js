// const layuiAll = require("../lib/layui/layui.all")

// 入口函数 -- dom树创建完毕就执行
$(function () {

  // 1.为按钮添加点击事件 ------------------------
  $('#link_reg').on('click', function () {
    $('.login-box').hide()
    $('.reg-box').show()
  })

  $('#link_login').on('click', function () {
    $('.login-box').show()
    $('.reg-box').hide()
  })

  // 2.为layui添加表单自定义规则 -------------------
  layui.form.verify({
    pwd: [/^[\S]{6,12}$/, '密码必须在6~12位之间~~'],
    repwd: function (value) {
      if (value != $('#txtPwd').val()) {
        return '两次输入不一致！'
      }
    }
  })

  // 3.注册表单提交事件 ----------------------------
  // 注意：当 layui 校验通过后，会提交表单，触发表单的提交
  $('#formReg').on('submit', function (e) {
    // 阻止默认提交
    e.preventDefault()
    //获取表单中的数据
    const data = {
      username: $('#username').val().trim(),
      password: $('#userpwd').val().trim()
    }

    $.post('/api/reguser', data, function (res) {
      //判断如果失败就返回错误信息
      if (res.status != 0) return layui.layer.msg(res.message)
      layer.msg(res.message)
      //模拟登录按钮被点击
      $('#link_login').trigger('click')
      $('#login_name').val(data.username)
      $('#formReg')[0].reset()
    })

  })
  // 4.登录表单的提交事件 -------------------------
  $('#formLogin').on('submit', function (e) {
    //阻止默认提交行为
    e.preventDefault()
    const data = layui.form.val('f1')
    $.post('/api/login', data, function (res) {
      if (res.status != 0) return layui.layer.msg(res.message)
      layer.msg('登录成功！', {
        icon: 1,
        time: 666 //2秒关闭（如果不配置，默认是3秒）
      }, function () {
        sessionStorage.setItem('token', res.token)
        location.replace('/index.html')
      });
    })
  })
})